using DelimitedFiles

function check_known(word, knowns)
    if isempty(knowns)
        return true
    end

    for (k, v) in knowns
        if v == '.'
            continue
        end
        if word[k] == only(v)
            continue
        else
            return false
        end
    end
    return true
end

function check_wrong(word, wrongs)
    if isempty(wrongs)
        return true
    end

    for wrong in wrongs
        l, i = wrong
        if word[i] == only(l)
            return false
        else
            continue
        end

    end

    return true
end

begin
    words = readdlm("wordle.txt")[:]

    keep = []
    for (index, word) in enumerate(words)
        if length(word) == 5
            push!(keep, index)
        end
    end

    words = lowercase.(words[keep])
end

# First Guesses
# PENIS
# SOARE 3.45
# RAISE 3.46
# ARISE 3.47
# SERAI 3.52

begin
    wrongs = []
    banned = "peniwtm" |> collect
    knowns = "su..." |> collect |> enumerate
    wrongs = [('s', 5), ('r', 3), ('s', 4)] # (letter, index)
    # contained = "s" |> collect
    @assert length(knowns) == 5
    contained = [contained; [w[1] for w in wrongs]]
    i = 0
    for word in words
        if any(occursin.(banned, word))
            continue
        end
        if all(occursin.(contained, word))
            if check_known(word, knowns)
                if check_wrong(word, wrongs)
                    println(word)
                    i += 1
                end
            end
        end
    end
    println("Possibilities: ", i)
end
